#include <gtest/gtest.h>
#include "messages/ClassX.hpp"

class ClassXTest : public testing::Test {
    public:
        std::vector<char> _testPayload {1, 2, 3, 4, 5};
        std::vector<char> _testRx {
            static_cast<char>(XType::CMD_MSG), 
            // Expected RX is big endian
            static_cast<char>((static_cast<uint16_t>(_testPayload.size()) >> 8) & 0xFF),
            static_cast<char>(static_cast<uint16_t>(_testPayload.size()) & 0xFF)};
        void SetUp() override { 
            Utils::vector_appender(_testRx, _testPayload);
        }
        void TearDown() override { }
};

TEST_F(ClassXTest, CtorTest) {
    ClassXMsg testMsg(_testPayload, XType::CMD_MSG);
    EXPECT_EQ(testMsg.getPayload(), _testPayload);
    EXPECT_EQ(testMsg.getHdrPayloadSize(), _testPayload.size());
    EXPECT_EQ(testMsg.getHeader().payloadSize, htons(_testPayload.size()));
    EXPECT_EQ(testMsg.getMsg(), _testRx);
}

TEST_F(ClassXTest, BuildTestGood) {
    auto builtMsg = buildClassXMsg(_testRx);
    ASSERT_TRUE(builtMsg);
    auto testMsg = *builtMsg;
    EXPECT_EQ(testMsg.getPayload(), _testPayload);
    EXPECT_EQ(testMsg.getHdrPayloadSize(), _testPayload.size());
    EXPECT_EQ(testMsg.getHeader().payloadSize, htons(_testPayload.size()));
    EXPECT_EQ(testMsg.getMsg(), _testRx);
}

TEST_F(ClassXTest, BuildTestBadSmall) {
    std::vector<char> testSmolMsg(1, 0);
    auto builtMsg = buildClassXMsg(testSmolMsg);
    ASSERT_FALSE(builtMsg);
}

TEST_F(ClassXTest, BuildTestMismatchSize) {
    _testRx[1] = 0x01;
    _testRx[2] = 0x02;
    auto builtMsg = buildClassXMsg(_testRx);
    ASSERT_FALSE(builtMsg);
}
