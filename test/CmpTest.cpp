#include <gtest/gtest.h>
#include "messages/Cmp.hpp"

class CmpTest : public testing::Test {
    public:
        std::vector<char> _testPayload {1, 2, 3, 4, 5};
        std::vector<char> _testRx {
            static_cast<char>(0x01),
            static_cast<char>((CmpType::INFO_MSG >> 8) & 0xFF), 
            static_cast<char>(CmpType::INFO_MSG & 0xFF), 
            static_cast<char>(0x03),
            // Expected RX is big endian
            static_cast<char>((static_cast<uint16_t>(_testPayload.size()) >> 8) & 0xFF),
            static_cast<char>(static_cast<uint16_t>(_testPayload.size()) & 0xFF)};
        void SetUp() override { 
            Utils::vector_appender(_testRx, _testPayload);
        }
        void TearDown() override { }
};

TEST_F(CmpTest, CtorTest) {
    CmpMsg testMsg(_testPayload, 0x01, 0x03, CmpType::INFO_MSG);
    EXPECT_EQ(testMsg.getHdrPayloadType(), CmpType::INFO_MSG);
    EXPECT_EQ(testMsg.getHdrVersion(), 0x01);
    EXPECT_EQ(testMsg.getHdrPayloadVersion(), 0x03);
    EXPECT_EQ(testMsg.getPayload(), _testPayload);
    EXPECT_EQ(testMsg.getHdrPayloadSize(), _testPayload.size());
    EXPECT_EQ(testMsg.getHeader().payloadSize, htons(_testPayload.size()));
    EXPECT_EQ(testMsg.getMsg(), _testRx);
}

TEST_F(CmpTest, BuildTestGood) {
    auto builtMsg = buildCmpMsg(_testRx);
    ASSERT_TRUE(builtMsg);
    auto testMsg = *builtMsg;
    EXPECT_EQ(testMsg.getHdrPayloadType(), CmpType::INFO_MSG);
    EXPECT_EQ(testMsg.getHdrVersion(), 0x01);
    EXPECT_EQ(testMsg.getHdrPayloadVersion(), 0x03);
    EXPECT_EQ(testMsg.getPayload(), _testPayload);
    EXPECT_EQ(testMsg.getHdrPayloadSize(), _testPayload.size());
    EXPECT_EQ(testMsg.getHeader().payloadSize, htons(_testPayload.size()));
    EXPECT_EQ(testMsg.getMsg(), _testRx);
}

TEST_F(CmpTest, BuildTestBadSmall) {
    std::vector<char> testSmolMsg(1, 0);
    auto builtMsg = buildCmpMsg(testSmolMsg);
    ASSERT_FALSE(builtMsg);
}

TEST_F(CmpTest, BuildTestMismatchSize) {
    _testRx[4] = 0x01;
    _testRx[5] = 0x02;
    auto builtMsg = buildCmpMsg(_testRx);
    ASSERT_FALSE(builtMsg);
}
