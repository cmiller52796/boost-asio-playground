set(TEST_BINARY ${PROJECT_NAME}Test)

add_executable(${TEST_BINARY} 
    main.cpp 
    TcpHandlerTest.cpp 
    ClassXtest.cpp 
    CmpTest.cpp 
)
target_include_directories(${TEST_BINARY} PRIVATE ${GTEST_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR}/include)
target_link_libraries(${TEST_BINARY} PRIVATE ${PROJECT_LIB} ${GTEST_LIBRARIES} pthread)

gtest_discover_tests(${TEST_BINARY})
