[requires]
    boost/1.84.0
    glog/0.7.0

[generators]
    CMakeDeps
    CMakeToolchain

[layout]
    cmake_layout
