# boost-asio-playground

A simple repository showing one way of using Boost ASIO, in this cast a TCP server that once it receives a message, 
posts events of its own with a corresponding vector of bytes via asio::post, so that designated handlers can act 
accordingly. This way, you can have multiple handlers respond to the same event in different ways.

## Building and Running Tests

`make build`  
This will pull down the cpp-dev-env I used for making this at the correct version and build the binary inside it.

`make test`  
This will pull down the cpp-dev-env I used for making this at the correct version and will run the unit tests.

`make clean`  
Will delete the build, Testing, and CMakeFiles directories
