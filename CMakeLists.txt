# CMake stuff based on tutorial info and this: https://palikar.github.io/posts/cmake_structure/
cmake_minimum_required(VERSION 3.24.3)
project(asioPlayground LANGUAGES CXX)
# set(CMAKE_LANGUAGES_COMPILER gcc)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED On)
# CCACHE to help compilation caching for speeeeed
find_program(CCACHE ccache)
if(CCACHE)
    set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE})
endif()

# TODO: CLI this from the makefile?
# set(CMAKE_BUILD_TYPE RELEASE)
# Can set different standards for different targets with
# set_property(TARGET {targetName} PROPERTY CXX_STANDARD #)

# Find packages needed for src and test executables, 
# version # is notated in conanfile.txt
find_package(Boost REQUIRED)
find_package(glog REQUIRED)
find_package(GTest REQUIRED)

# Make a common lib object to be shared between src
# and test executables
set(PROJECT_LIB ${PROJECT_NAME}Lib)
add_library(${PROJECT_LIB}
    STATIC 
    src/handlers/PrintHandler.cpp 
    src/handlers/TcpHandler.cpp
)
target_include_directories(${PROJECT_LIB} PUBLIC ${CMAKE_SOURCE_DIR}/include)
target_link_libraries(${PROJECT_LIB} PUBLIC Boost::boost glog::glog)

add_subdirectory(src bin)
enable_testing()
add_subdirectory(test)
