#include "Utils.hpp"
#include "handlers/PrintHandler.hpp"

PrintHandler::PrintHandler(std::shared_ptr<EventHub> hub): 
    _hubPtr(hub) 
{

}

void PrintHandler::init() {
    auto ptr = _hubPtr.lock();
    if(not ptr) { return; }
    ptr->subscribe(shared_from_this(), MsgEvent::RECEIVE);
}

void PrintHandler::process_event(MsgEvent subId, const std::vector<char>& msg) {
    switch(subId) {
        case MsgEvent::RECEIVE:
            print_msg(msg);
            break;
        default:
            break;
    }
}

void PrintHandler::print_msg(const std::vector<char>& msg) {
    auto ptr = _hubPtr.lock();
    if (not ptr) { return; }
    LOG(INFO) << "Msg received " << msg;
    ptr->publish(MsgEvent::SEND, std::vector<char>{'O', 'I', ' ', 'M', 'A', 'T', 'E'});
}
