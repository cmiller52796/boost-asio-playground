#include "handlers/TcpHandler.hpp"

TcpHandler::TcpHandler(std::shared_ptr<EventHub> hub, io_context& io): 
    _hubPtr(hub), 
    _socket(io),
    _acceptor(io)
{ }

void TcpHandler::init() {
    auto ptr = _hubPtr.lock();
    if (not ptr) { return; }
    ptr->subscribe(shared_from_this(), MsgEvent::SEND);
    await_connections();
}

void TcpHandler::process_event(MsgEvent subId, const std::vector<char>& msg) {
    switch(subId) {
        case MsgEvent::SEND:
            send_msg(msg);
            break;
        default:
            break;
    }
}

void TcpHandler::await_connections() {
    if (not _acceptor.is_open()) {
        ip::tcp::endpoint endpoint(ip::tcp::v4(), 55555);
        _acceptor.open(endpoint.protocol(), _ec);
        if(_ec) { LOG(FATAL) << "Open error:" << _ec.message(); }
        _acceptor.set_option(ip::tcp::acceptor::reuse_address(true), _ec);
        if(_ec) { LOG(FATAL) << "Reuse opt error:" << _ec.message(); }
        _acceptor.bind(endpoint, _ec);
        if(_ec) { LOG(FATAL) << "Bind error:" << _ec.message(); }
        // Allowing only one client at a time, so set pending queue to 0
        _acceptor.listen(0, _ec);
        if(_ec) { LOG(FATAL) << "Listen error:" << _ec.message(); }
    }
    // Capture weak pointer in lambda as to not have it increment reference
    // count of outer shared pointer
    LOG(INFO) << "Awaiting connection..."; 
    _acceptor.async_accept(_socket, 
        [selfWeak = std::weak_ptr<TcpHandler>(shared_from_this())]
        (const boost::system::error_code& ec) { 
            auto ptr = selfWeak.lock();
            if (not ptr) { return; }
            ptr->handle_accept(ec);
        }
        );
}

void TcpHandler::handle_accept(const boost::system::error_code& ec) {
    if(ec) { 
        VLOG(1) << "Accept error:" << ec.message(); 
        _acceptor.cancel();
        _acceptor.close();
        await_connections();
    }
    // Allowing only one client at a time
    _acceptor.close();
    VLOG(1) << "Connection accepted, IP: " 
        << _socket.remote_endpoint().address().to_string() 
        << " Port: " << _socket.remote_endpoint().port(); 
    await_header();
}

void TcpHandler::await_header() {
    VLOG(3) << "Awaiting header..."; 
    async_read(_socket, buffer(_arrayBuf.data(), sizeof(ClassXHeader)), 
        [selfWeak = std::weak_ptr<TcpHandler>(shared_from_this())]
        (const boost::system::error_code& ec, std::size_t bytes_transferred) {
            if(ec) { LOG(ERROR) << "Async Read Error: " << ec.message(); }
            VLOG(3) << "Server received header, size: " << bytes_transferred;
            auto ptr = selfWeak.lock();
            if (not ptr) { return; }
            ptr->await_msg();
        });
}

void TcpHandler::await_msg() {
    ClassXHeader header {};
    memcpy(&header, _arrayBuf.data(), sizeof(ClassXHeader));
    VLOG(3) << "Awaiting message...";
    // Trigger this after one we get the rest of the message
    async_read(_socket, buffer(_arrayBuf.data() + sizeof(ClassXHeader), header.getPayloadSize()), 
        [selfWeak = std::weak_ptr<TcpHandler>(shared_from_this())]
        (const boost::system::error_code& ec, std::size_t bytes_transferred) {
            if(ec) { LOG(ERROR) << "Async Read Error: " << ec.message(); }
            VLOG(3) << "Server received header, size: " << bytes_transferred;
            auto ptr = selfWeak.lock();
            if (not ptr) { return; }
            ptr->forward_msg(bytes_transferred + sizeof(ClassXHeader));
        });
}

void TcpHandler::forward_msg(size_t msgSize) {
    auto hubPtr = _hubPtr.lock();
    if (not hubPtr) { return; }
    auto ClassXMsg = buildClassXMsg(std::vector<char>(_arrayBuf.begin(), _arrayBuf.begin() + msgSize));
    if(not ClassXMsg) { return; }
    VLOG(3) << "Class X message found, forwarding content...";
    // Forward off the payload only, which should be one or more CMP messages
    hubPtr->publish(MsgEvent::RECEIVE, (*ClassXMsg).getPayload());
    await_header();
}

void TcpHandler::send_msg(const std::vector<char>& msg) { }
