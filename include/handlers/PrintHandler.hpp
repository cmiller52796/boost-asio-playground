#pragma once

#include "EventHandler.hpp"
#include "EventHub.hpp"
#include <memory>
#include <glog/logging.h>

class PrintHandler : public std::enable_shared_from_this<PrintHandler>, public EventHandler {
    public:
        PrintHandler(std::shared_ptr<EventHub> hub);
        ~PrintHandler() = default;
        void init();
        void process_event(MsgEvent subId, const std::vector<char>& msg) override;

    private:
        void print_msg(const std::vector<char>& msg);
        std::weak_ptr<EventHub> _hubPtr;
};
