#pragma once

#include "EventHandler.hpp"
#include "EventHub.hpp"
#include "messages/ClassX.hpp"
#include <memory>
#include <vector>
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <glog/logging.h>

using namespace boost::asio;

class TcpHandler : public std::enable_shared_from_this<TcpHandler>, public EventHandler {
    public:
        TcpHandler(std::shared_ptr<EventHub> hub, io_context& io);
        ~TcpHandler() = default;

        void init();
        void process_event(MsgEvent subId, const std::vector<char>& msg) override;
    private:
        void await_connections();
        void handle_accept(const boost::system::error_code& ec);
        void await_header();
        void await_msg();
        void forward_msg(size_t msgSize);
        void send_msg(const std::vector<char>& msg);

        ClassXHeader _classXHeader;
        boost::system::error_code _ec;
        ip::tcp::socket _socket;
        ip::tcp::acceptor _acceptor;
        std::array<char, 4096> _arrayBuf;
        std::weak_ptr<EventHub> _hubPtr;
};
