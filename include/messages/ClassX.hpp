#pragma once
#include <stdint.h>
#include <optional>
#include <arpa/inet.h>
#include <glog/logging.h>
#include "Utils.hpp"

#pragma pack(1)
struct ClassXHeader {
    uint8_t payloadType;
    uint16_t payloadSize;
    // This is expected to be received over
    // a network, so account for endianness
    const uint16_t getPayloadSize() {
        return ntohs(payloadSize);
    }
};

enum class XType : uint8_t {
    INFO_MSG=5,
    CMD_MSG=6,
    KEEP_ALIVE_MESSAGE=7
};

class ClassXMsg {
    public:
        // Don't provide a CTOR where a header is passed in, 
        // always construct such to calculate size for the header
        // based on payload
        ClassXMsg(std::vector<char> payload, XType payload_type) : 
            _hdr(ClassXHeader{.payloadType = static_cast<uint8_t>(payload_type), 
                .payloadSize=htons(static_cast<uint16_t>(payload.size()))}),
            _payload(payload) { 
                populateMsg();
            }
        ~ClassXMsg() = default;

        const std::vector<char> getMsg() {
            return _msg;
        }

        const std::vector<char> getPayload() {
            return _payload;
        }

        const ClassXHeader getHeader() {
            return _hdr;
        }

        const uint16_t getHdrPayloadSize() {
            return _hdr.getPayloadSize();
        }

        const uint8_t getHdrPayloadType() {
            return _hdr.payloadType;
        }

    private:
        void populateMsg() {
            _msg.push_back(static_cast<char>(_hdr.payloadType));
            // payloadSize in header is endian swapped already, so push its
            // low byte first, then high
            _msg.push_back(static_cast<char>(_hdr.payloadSize & 0xFF));
            _msg.push_back(static_cast<char>((_hdr.payloadSize >> 8) & 0xFF));
            Utils::vector_appender(_msg, _payload);
        }

        ClassXHeader _hdr;
        std::vector<char> _payload;
        std::vector<char> _msg;
};

static std::optional<ClassXMsg> buildClassXMsg(const std::vector<char>& bytes) {
    // Shouldn't have a header only message, ever
    if(bytes.size() <= sizeof(ClassXHeader)) { 
        LOG(WARNING) << "Byte size smaller than ClassX Header: " << bytes.size();
        return {}; 
    }
    // TODO: Find something cleaner, having to access by struct is good because
    // it guarantees we are following format, but seems wasteful to make this object
    // just to poke a couple bytes..
    ClassXHeader classXHeader {};
    memcpy(&classXHeader, bytes.data(), sizeof(ClassXHeader));
    if(classXHeader.getPayloadSize() != (bytes.size() - sizeof(ClassXHeader))) {
        LOG(WARNING) << "Byte size mismatch: " 
            << classXHeader.getPayloadSize() 
            << " vs " 
            << (bytes.size() - sizeof(ClassXHeader));
        return {};
    }
    return ClassXMsg(std::vector<char>(bytes.begin() + sizeof(ClassXHeader), bytes.end()), 
        static_cast<XType>(classXHeader.payloadType));
}
