#pragma once
#include <stdint.h>
#include <optional>
#include <arpa/inet.h>
#include <glog/logging.h>
#include "Utils.hpp"

#pragma pack(1)
struct CmpHeader {
    uint8_t headerVersion;
    uint16_t payloadType;
    uint8_t payloadVersion;
    uint16_t payloadSize;
    const uint16_t getPayloadType() {
        return ntohs(payloadType);
    }
    // This is expected to be received over
    // a network, so account for endianness
    const uint16_t getPayloadSize() {
        return ntohs(payloadSize);
    }
};

enum CmpType : uint16_t {
    INFO_MSG=650,
    HELP_MSG=651,
    CMD_MSG=652,
    KEEP_ALIVE_MESSAGE=700
};

class CmpMsg {
    public:
        CmpMsg(std::vector<char> payload, uint8_t header_version, 
            uint8_t payload_version, CmpType payload_type) : 
            _hdr(CmpHeader{.headerVersion = header_version,
                .payloadType = htons(static_cast<uint16_t>(payload_type)), 
                .payloadVersion = payload_version,
                .payloadSize = htons(static_cast<uint16_t>(payload.size()))}),
            _payload(payload) { 
                populateMsg();
            }
        ~CmpMsg() = default;

        const std::vector<char> getMsg() {
            return _msg;
        }

        const std::vector<char> getPayload() {
            return _payload;
        }

        const CmpHeader getHeader() {
            return _hdr;
        }

        const uint8_t getHdrVersion() {
            return _hdr.headerVersion;
        }

        const uint8_t getHdrPayloadVersion() {
            return _hdr.payloadVersion;
        }

        const uint16_t getHdrPayloadSize() {
            return _hdr.getPayloadSize();
        }

        const uint16_t getHdrPayloadType() {
            return _hdr.getPayloadType();
        }

    private:
        void populateMsg() {
            _msg.push_back(static_cast<char>(_hdr.headerVersion));
            // payloadType in header is endian swapped already, so push its
            // low byte first, then high
            _msg.push_back(static_cast<char>(_hdr.payloadType & 0xFF));
            _msg.push_back(static_cast<char>((_hdr.payloadType >> 8) & 0xFF));
            _msg.push_back(static_cast<char>(_hdr.payloadVersion));
            // payloadSize in header is endian swapped already, so push its
            // low byte first, then high
            _msg.push_back(static_cast<char>(_hdr.payloadSize & 0xFF));
            _msg.push_back(static_cast<char>((_hdr.payloadSize >> 8) & 0xFF));
            Utils::vector_appender(_msg, _payload);
        }

        CmpHeader _hdr;
        std::vector<char> _payload;
        std::vector<char> _msg;
};

static std::optional<CmpMsg> buildCmpMsg(const std::vector<char>& bytes) {
    // Shouldn't have a header only message, ever
    if(bytes.size() <= sizeof(CmpHeader)) { 
        LOG(WARNING) << "Byte size smaller than Cmp Header: " << bytes.size();
        return {}; 
    }
    CmpHeader cmpHeader {};
    memcpy(&cmpHeader, bytes.data(), sizeof(CmpHeader));
    if(cmpHeader.getPayloadSize() != (bytes.size() - sizeof(CmpHeader))) {
        LOG(WARNING) << "Byte size mismatch: " 
            << cmpHeader.getPayloadSize() 
            << " vs " 
            << (bytes.size() - sizeof(CmpHeader));
        return {};
    }
    return CmpMsg(std::vector<char>(bytes.begin() + sizeof(CmpHeader), bytes.end()), 
        cmpHeader.headerVersion, cmpHeader.payloadVersion, 
        static_cast<CmpType>(cmpHeader.getPayloadType()));
}
