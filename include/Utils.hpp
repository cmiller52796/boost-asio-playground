#pragma once

#include <vector>
#include <iostream>

inline std::ostream& operator<<(std::ostream& os, const std::vector<char>& vec) {
    os << "\n";
    for (const auto& ch : vec) {
        os << "0x" << +ch << " ";
    }
    return os;
}

class Utils {
    public:
        static inline void vector_appender(std::vector<char>& appendingTo, const std::vector<char>& appendMe) {
            appendingTo.insert(appendingTo.end(), appendMe.begin(), appendMe.end());
        }
};
